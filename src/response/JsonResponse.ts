import { Injectable } from '@nestjs/common';
import { BaseResponse } from './BaseResponse';

@Injectable()
export class JsonResponse extends BaseResponse {
  success(data: any, code: number) {
    return {
      success: true,
      data,
      code,
    };
  }

  error(data: any, code: number, error: any) {
    return {
      success: true,
      data,
      code,
      error,
    };
  }
}
