import { Injectable } from '@nestjs/common';

@Injectable()
export abstract class BaseResponse {
  abstract success(data: any, code: number);
  abstract error(data: any, code: number, error: any);
}
