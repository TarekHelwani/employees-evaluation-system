export const jwtConstants = {
  secret: 'thisistheSECRETstringthatshouldbekeptinsecret:)',
  expiresIn: 60 * 60 * 24,
};
