export const ReviewTypes = {
  1: 'Productivity',
  2: 'Attendance',
  3: 'Engagement',
  4: 'Social',
  5: 'Learning',
};
