import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const port = 5000;
  await app.listen(port);
  console.log(`Running on http://localhost:${port}`);
}

bootstrap();
