import { Controller, Get, Param, Query } from '@nestjs/common';
import { ReportsService } from './reports.service';
import { Employee } from '../employees/employee.entity';

@Controller('reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  @Get('employees-with-ranks')
  async getEmployeesWithRanks(): Promise<Employee[]> {
    return await this.reportsService.getEmployeesWithRanks();
  }

  @Get('advanced')
  async getAdvancedData(): Promise<Employee[]> {
    return await this.reportsService.getAdvancedData();
  }

  @Get(':id')
  async findOne(
    @Param('id') employeeId,
    @Query('startDate') startDate,
    @Query('endDate') endDate,
    @Query('bestReview') bestReview,
    @Query('worstReview') worstReview,
  ): Promise<Employee> {
    return this.reportsService.getReport(
      employeeId,
      startDate,
      endDate,
      bestReview,
      worstReview,
    );
  }
}
