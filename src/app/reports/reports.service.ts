import { Injectable, Inject } from '@nestjs/common';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import { Review, SubReview } from '../reviews/review.entity';
import { Employee } from '../employees/employee.entity';
import sequelize from 'sequelize';
import { Op } from 'sequelize';
import { Level } from '../categories/categories.entity';

@Injectable()
export class ReportsService {
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
  ) {}

  async getReport(
    employeeId: number,
    startDate: string,
    endDate: string,
    bestReview: boolean,
    worstReview: boolean,
  ): Promise<Employee> {
    return new Promise<Employee>(
      // eslint-disable-next-line @typescript-eslint/ban-types
      (resolve: Function, reject: Function) => {
        return Employee.findOne({
          include: [
            {
              model: Review,
              where:
                startDate && endDate
                  ? {
                      createdAt: {
                        [Op.between]: [new Date(startDate), new Date(endDate)],
                      },
                      rank: {
                        [Op.eq]: sequelize.literal(`select max(reviews)`),
                      },
                    }
                  : {},
              include: [
                {
                  model: SubReview,
                },
              ],
            },
          ],
          where: {
            id: employeeId,
          },
        })
          .then((employee: Employee) => {
            if (employee) {
              this.logger.info(
                `Retrieved employee with name ${employee.name}.`,
              );
              let totalSubReviews = 0;
              let totalRank = 0;
              employee.reviews.forEach((review: Review) => {
                review.subReviews?.forEach((subReview: SubReview) => {
                  totalSubReviews += 1;
                  totalRank += subReview.rank;
                });
              });
              employee.setDataValue('totalRank', totalRank);
              employee.setDataValue('totalSubReviews', totalSubReviews);
            } else {
              this.logger.info(`Employee does not exist.`);
            }
            resolve(employee);
          })
          .catch((error: Error) => {
            this.logger.error(error.message);
            reject(error);
          });
      },
    );
  }

  async getEmployeesWithRanks(): Promise<Employee[]> {
    try {
      return await Employee.findAll({
        attributes: [
          'id',
          'name',
          'level_id',
          [sequelize.fn('SUM', sequelize.col('reviews.rank')), 'rank'],
        ],
        include: [
          {
            model: Review,
            required: false,
            attributes: [],
          },
          {
            model: Level,
            required: true,
            attributes: ['name'],
          },
        ],
        group: ['Employee.id'],
      });
    } catch (error) {
      this.logger.error(error.message);
    }
  }

  async getAdvancedData(): Promise<Employee[]> {
    try {
      return await Employee.findAll({
        include: [
          {
            model: Review,
            required: false,
            include: [
              {
                model: SubReview,
              },
            ],
          },
        ],
      });
    } catch (error) {
      this.logger.error(error.message);
    }
  }
}
