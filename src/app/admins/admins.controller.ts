import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  UseGuards,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { AdminsService } from './admins.service';
import { CreateAdminDto } from './dto/create-admin-dto';
import { Admin } from './admin.entity';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('admins')
export class AdminsController {
  constructor(private readonly adminsService: AdminsService) {}

  // @Get()
  // async findAll() {
  //   const res = await this.adminsService.findAll();
  //   return this.response.success(res, ResponseCodes.OK);
  // }
  @Get()
  async findAll(): Promise<Admin[]> {
    return await this.adminsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id): Promise<Admin> {
    return await this.adminsService.findOne(id);
  }

  @Post()
  async create(
    @Body('name') name: string,
    @Body('email') email: string,
    @Body('password') password: string,
  ): Promise<Admin> {
    // validate admin data
    const hashedPassword = await bcrypt.hash(password, 12);
    return await this.adminsService.create(
      new CreateAdminDto(name, email, hashedPassword),
    );
  }

  @Delete(':id')
  async delete(@Param('id') id): Promise<any> {
    const res = await this.adminsService.delete(id);
    return { message: res ? 'Success' : 'An error occured' };
  }

  @Put(':id')
  async update(
    @Body('name') name: string,
    @Body('email') email: string,
    @Body('password') password: string,
    @Param('id') id,
  ): Promise<Admin> {
    // validate data
    const updateAdminDto = new CreateAdminDto(
      name,
      email,
      password ? await bcrypt.hash(password, 12) : undefined,
    );
    return await this.adminsService.update(id, updateAdminDto);
  }
}
