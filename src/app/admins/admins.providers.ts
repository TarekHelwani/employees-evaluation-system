import { Admin } from './admin.entity';
import { Providers } from '../../constants';
import { JsonResponse } from 'src/response/JsonResponse';

export const adminProviders = [
  {
    provide: Providers.ADMIN_REPO,
    useValue: Admin,
  },
  {
    provide: Providers.BASE_RESPONSE,
    useClass: JsonResponse,
  },
];
