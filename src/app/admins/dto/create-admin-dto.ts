export class CreateAdminDto {
  readonly name: string;
  readonly email: string;
  readonly password: string;
  readonly created_at: Date;
  readonly updated_at: Date;

  constructor(name: string, email: string, password: string) {
    this.name = name;
    this.email = email;
    this.password = password;
  }
}
