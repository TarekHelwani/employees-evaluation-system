import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../database/database.module';
import { AdminsService } from './admins.service';
import { AdminsController } from './admins.controller';
import { adminProviders } from './admins.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [AdminsController],
  providers: [AdminsService, ...adminProviders],
  exports: [AdminsService],
})
export class AdminsModule {}
