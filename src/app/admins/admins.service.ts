import { Injectable, Inject } from '@nestjs/common';
import { Providers } from '../../constants';
import { Admin } from './admin.entity';
import { Logger } from 'winston';
import { CreateAdminDto } from './dto/create-admin-dto';

@Injectable()
export class AdminsService {
  constructor(
    @Inject(Providers.ADMIN_REPO)
    private adminsRepository: typeof Admin,
    @Inject('winston')
    private readonly logger: Logger,
  ) {}

  async findAll(): Promise<Admin[]> {
    try {
      return await this.adminsRepository.findAll();
    } catch (error) {
      this.logger.error(error.message);
    }
  }

  async findOne(id: number): Promise<Admin> {
    try {
      const admin = await this.adminsRepository.findOne({ where: { id } });
      admin
        ? this.logger.info(`Retrieved admin with name ${admin.name}.`)
        : this.logger.info(`Admin with name ${admin.name} does not exist.`);
      return admin;
    } catch (error) {
      this.logger.error(error.message);
    }
  }

  async findByEmail(email: string): Promise<Admin> {
    try {
      const admin = await this.adminsRepository.findOne({ where: { email } });
      admin
        ? this.logger.info(`Retrieved admin with name ${admin.name}.`)
        : this.logger.info(`Admin with name ${admin.name} does not exist.`);
      return admin;
    } catch (error) {
      this.logger.error(error.message);
    }
  }

  async create(createAdminDto: CreateAdminDto): Promise<Admin> {
    try {
      const admin = await this.adminsRepository.create(createAdminDto);
      this.logger.info(`Created admin with name ${admin.name}.`);
      return admin;
    } catch (error) {
      this.logger.error(error.message);
    }
  }

  async delete(id: number): Promise<boolean> {
    try {
      const res = await this.adminsRepository.destroy({ where: { id } });
      res > 0
        ? this.logger.info(`Deleted admin with id ${id}`)
        : this.logger.info(`Admin with id ${id} does not exist.`);
      return !!res;
    } catch (error) {
      this.logger.error(error.message);
    }
  }

  async update(id: number, updateAdminDto: CreateAdminDto): Promise<Admin> {
    try {
      const admin = await this.adminsRepository.findOne({ where: { id } });
      if (admin) {
        await admin.update(updateAdminDto);
        this.logger.info(`Updated admin with name ${admin.name}`);
        this.logger.info(`Admin with id ${admin.name} does not exist.`);
      }
      return admin;
    } catch (error) {
      this.logger.error(error.message);
    }
  }
}
