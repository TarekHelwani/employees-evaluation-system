import {
  AutoIncrement,
  BelongsTo,
  Column,
  ForeignKey,
  HasMany,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';
import { Level } from '../categories/categories.entity';
import { EmployeeInfo } from '../../constants';
import { Review } from '../reviews/review.entity';

@Table
export class Employee extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string;

  @ForeignKey(() => Level)
  @Column
  level_id: number;

  @BelongsTo(() => Level, {
    onDelete: 'CASCADE',
  })
  level: Level;

  @HasMany(() => Review, {
    onDelete: 'CASCADE',
  })
  reviews: Review[];

  getEmployeeSalary(): number {
    return EmployeeInfo.BASE_SALARY * this.level.weight;
  }
}
