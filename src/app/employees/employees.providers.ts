import { Employee } from './employee.entity';
import { Providers } from '../../constants';

export const employeesProviders = [
  {
    provide: Providers.EMPLOYEE_REPO,
    useValue: Employee,
  },
];
