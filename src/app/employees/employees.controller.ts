import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  Query,
} from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { CreateEmployeeDto } from './dto/create-employee-dto';
import { Employee } from './employee.entity';

@Controller('employees')
export class EmployeesController {
  constructor(private readonly employeesService: EmployeesService) {}

  @Get()
  async findAll(@Query('category') categoryId): Promise<Employee[]> {
    return this.employeesService.findAll(categoryId);
  }

  @Get(':id')
  async findOne(@Param('id') id): Promise<Employee> {
    return this.employeesService.findOne(id);
  }

  @Post('/')
  create(@Body() createEmployeeDto: CreateEmployeeDto): Promise<Employee> {
    return this.employeesService.create(createEmployeeDto);
  }

  @Delete(':id')
  delete(@Param('id') id): Promise<Employee> {
    return this.employeesService.delete(id);
  }

  @Put(':id')
  update(
    @Body() updateEmployeeDto: CreateEmployeeDto,
    @Param('id') id,
  ): Promise<Employee> {
    return this.employeesService.update(id, updateEmployeeDto);
  }
}
