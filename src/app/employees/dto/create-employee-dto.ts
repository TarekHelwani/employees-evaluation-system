export class CreateEmployeeDto {
  readonly name: string;
  readonly level_id: number;
}
