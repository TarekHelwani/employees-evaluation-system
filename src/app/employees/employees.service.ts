import { Injectable, Inject } from '@nestjs/common';
import { Providers } from '../../constants';
import { Employee } from './employee.entity';
import { CreateEmployeeDto } from './dto/create-employee-dto';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import { Category, Level } from '../categories/categories.entity';
import { EmployeeInfo } from '../../constants';

@Injectable()
export class EmployeesService {
  constructor(
    @Inject(Providers.EMPLOYEE_REPO)
    private employeesRepository: typeof Employee,
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
  ) {}

  async findAll(categoryId: number): Promise<Employee[]> {
    return new Promise<Array<Employee>>(
      // eslint-disable-next-line @typescript-eslint/ban-types
      (resolve: Function, reject: Function) => {
        return Employee.findAll({
          include: [
            {
              model: Level,
              required: true,
              include: categoryId
                ? [
                    {
                      model: Category,
                      where: { id: categoryId },
                    },
                  ]
                : [],
            },
          ],
        })
          .then((employees: Array<Employee>) => {
            employees.forEach((employee: Employee) => {
              employee.setDataValue('salary', employee.getEmployeeSalary());
            });
            this.logger.info(`Retrieved employees.`);
            resolve(employees);
          })
          .catch((error: Error) => {
            this.logger.error(error.message);
            reject(error);
          });
      },
    );
  }

  async findOne(id: number) {
    return new Promise<Employee>(
      // eslint-disable-next-line @typescript-eslint/ban-types
      (resolve: Function, reject: Function) => {
        return Employee.findOne({
          include: [
            {
              model: Level,
            },
          ],
          where: { id: id },
        })
          .then((employee: Employee) => {
            if (employee) {
              this.logger.info(
                `Retrieved employee with name ${employee.name}.`,
              );
            } else {
              this.logger.info(
                `Product with name ${employee.name} does not exist.`,
              );
            }
            employee.setDataValue('salary', employee.getEmployeeSalary());
            resolve(employee);
          })
          .catch((error: Error) => {
            this.logger.error(error.message);
            reject(error);
          });
      },
    );
  }

  async create(createEmployeeDto: CreateEmployeeDto): Promise<Employee> {
    return new Promise<Employee>(
      // eslint-disable-next-line @typescript-eslint/ban-types
      (resolve: Function, reject: Function) => {
        return Employee.create(createEmployeeDto)
          .then((employee: Employee) => {
            this.logger.info(`Created product with name ${employee.name}.`);
            resolve(employee);
          })
          .catch((error: Error) => {
            this.logger.error(error.message);
            reject(error);
          });
      },
    );
  }

  async delete(id: number): Promise<Employee> {
    return new Promise<Employee>(
      // eslint-disable-next-line @typescript-eslint/ban-types
      (resolve: Function, reject: Function) => {
        return Employee.findOne({ where: { id: id } }).then(
          (employee: Employee) => {
            return Employee.destroy({ where: { id: id } })
              .then((affectedRows: number) => {
                if (affectedRows > 0) {
                  this.logger.info(
                    `Deleted employee with name ${employee.name}`,
                  );
                } else {
                  this.logger.info(
                    `Employee with name ${employee.name} does not exist.`,
                  );
                }
                resolve(employee);
              })
              .catch((error: Error) => {
                this.logger.error(error.message);
                reject(error);
              });
          },
        );
      },
    );
  }

  async update(
    id: number,
    updateEmployeeDto: CreateEmployeeDto,
  ): Promise<Employee> {
    return new Promise<Employee>(
      // eslint-disable-next-line @typescript-eslint/ban-types
      (resolve: Function, reject: Function) => {
        return Employee.update(updateEmployeeDto, { where: { id: id } })
          .then((results: [number, Array<Employee>]) => {
            if (results.length > 0) {
              this.logger.info(
                `Updated employee with name ${updateEmployeeDto.name}`,
              );
            } else {
              this.logger.info(
                `Employee with id ${updateEmployeeDto.name} does not exist.`,
              );
            }
          })
          .then(() => {
            resolve(Employee.findOne({ where: { id: id } }));
          })
          .catch((error: Error) => {
            this.logger.error(error.message);
            reject(error);
          });
      },
    );
  }
}
