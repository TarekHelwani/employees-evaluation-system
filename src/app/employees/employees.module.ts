import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../database/database.module';
import { EmployeesService } from './employees.service';
import { EmployeesController } from './employees.controller';
import { employeesProviders } from './employees.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [EmployeesController],
  providers: [EmployeesService, ...employeesProviders],
})
export class EmployeesModule {}
