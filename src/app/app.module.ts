import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EmployeesModule } from './employees/employees.module';
import { WinstonModule } from 'nest-winston';
import { transports } from 'winston';
import { loggingConfig } from '../config/logging-config';
import * as path from 'path';
import { CategoriesModule } from './categories/categories.module';
import { ReviewsModule, SubReviewsModule } from './reviews/reviews.module';
import { PromotionsModule } from './promotions/promotions.module';
import { AdminsModule } from './admins/admins.module';
import { AuthModule } from './auth/auth.module';
import { ReportsModule } from './reports/reports.module';

const config = loggingConfig;
config.file.filename = `${path.join(
  config.directory,
  '../employees-evaluation-system-logs',
)}/${config.file.filename}`;

@Module({
  imports: [
    EmployeesModule,
    AdminsModule,
    AuthModule,
    CategoriesModule,
    ReviewsModule,
    SubReviewsModule,
    PromotionsModule,
    ReportsModule,
    WinstonModule.forRoot({
      transports: [
        new transports.File(config.file),
        new transports.Console(config.console),
      ],
      exitOnError: false,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
