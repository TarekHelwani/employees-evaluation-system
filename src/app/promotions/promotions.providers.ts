import { Providers } from '../../constants';
import { Promotion } from './promotion.entity';

export const promotionsProviders = [
  {
    provide: Providers.PROMOTION_REPO,
    useValue: Promotion,
  },
];
