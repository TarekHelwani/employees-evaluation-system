import { Injectable, Inject } from '@nestjs/common';
import { Providers } from '../../constants';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import { Promotion } from './promotion.entity';
import { Employee } from '../employees/employee.entity';
import { Category, Level } from '../categories/categories.entity';

@Injectable()
export class PromotionsService {
  constructor(
    @Inject(Providers.PROMOTION_REPO)
    private promotionsRepository: typeof Promotion,
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
  ) {}

  async promote(employeeId: number): Promise<void> {
    return new Promise<void>(
      // eslint-disable-next-line @typescript-eslint/ban-types
      (resolve: Function, reject: Function) => {
        return Employee.findOne({
          where: { id: employeeId },
          include: [
            {
              model: Level,
              required: true,
            },
          ],
        })
          .then(async (employee: Employee) => {
            Level.findOne({
              include: [
                {
                  model: Category,
                  required: true,
                  where: {
                    id: employee.level.category_id,
                  },
                },
              ],
              where: { weight: employee.level.id + 1 },
            }).then((level: Level) => {
              if (level) {
                employee.update({
                  level_id: level.id,
                });
                this.logger.info(`${employee.name} has been promoted!`);
                Promotion.create({
                  employee_id: employeeId,
                }).then((promotion: Promotion) => {
                  resolve(promotion);
                });
              } else {
                resolve({
                  message: `${employee.name} cannot be promoted anymore!`,
                });
              }
            });
          })
          .catch((error: Error) => {
            this.logger.error(error.message);
            reject(error);
          });
      },
    );
  }
}
