import { Controller, Post, Query } from '@nestjs/common';
import { PromotionsService } from './promotions.service';

@Controller('promote')
export class PromotionsController {
  constructor(private readonly promotionsService: PromotionsService) {}

  @Post()
  promote(@Query('employee') employeeId): Promise<void> {
    return this.promotionsService.promote(employeeId);
  }
}
