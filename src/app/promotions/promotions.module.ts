import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../database/database.module';
import { PromotionsController } from './promotions.controller';
import { PromotionsService } from './promotions.service';
import { promotionsProviders } from './promotions.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [PromotionsController],
  providers: [PromotionsService, ...promotionsProviders],
})
export class PromotionsModule {}
