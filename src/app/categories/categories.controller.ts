import { Delete } from '@nestjs/common';
import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { Category } from './categories.entity';
import { CategoriesService } from './categories.service';
import { CreateCategoryDto } from './dto/create-category-dto';

@Controller('categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @Get()
  async findAll(): Promise<Category[]> {
    return this.categoriesService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id): Promise<Category> {
    return this.categoriesService.findOne(id);
  }

  @Post()
  async store(@Body() data: CreateCategoryDto) {
    return this.categoriesService.create(data);
  }

  @Delete(':id')
  async update(@Param('id') id) {
    return this.categoriesService.delete(id);
  }
}
