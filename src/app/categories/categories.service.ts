import { Injectable, Inject } from '@nestjs/common';
import { Providers } from '../../constants';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import { Category, Level } from './categories.entity';
import { CreateCategoryDto } from './dto/create-category-dto';

@Injectable()
export class CategoriesService {
  constructor(
    @Inject(Providers.CATEGORY_REPO)
    private categoriesRepository: typeof Category,
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
  ) {}

  async findAll(): Promise<Category[]> {
    // return new Promise<Array<Category>>(
    //   // eslint-disable-next-line @typescript-eslint/ban-types
    //   (resolve: Function, reject: Function) => {
    //     return Category.findAll()
    //       .then((categories: Array<Category>) => {
    //         this.logger.info(`Retrieved all categories.`);
    //         resolve(categories);
    //       })
    //       .catch((error: Error) => {
    //         this.logger.error(error.message);
    //         reject(error);
    //       });
    //   },
    // );

    return await Category.findAll({ include: Level });
  }

  async findOne(id: number): Promise<Category> {
    const category = await Category.findOne({
      where: { id },
      include: [Level],
    });
    return category;
  }

  async create(data: CreateCategoryDto) {
    const category = await Category.create({ ...data }, { include: Level });
    return category;
  }

  async delete(id: any) {
    const category = await Category.destroy({ where: { id } });
    return category;
  }
}
