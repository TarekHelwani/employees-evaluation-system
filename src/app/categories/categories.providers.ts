import { Providers } from '../../constants';
import { Category } from './categories.entity';

export const categoriesProviders = [
  {
    provide: Providers.CATEGORY_REPO,
    useValue: Category,
  },
];
