import { Level } from '../categories.entity';
import { CreateLevelDto } from './create-level-dto';

export class CreateCategoryDto {
  readonly name: string;
  readonly levels: CreateLevelDto[];
}
