import { Category } from '../categories.entity';
import { Employee } from '../../employees/employee.entity';

export class CreateLevelDto {
  readonly weight: number;
  readonly name: string;
}
