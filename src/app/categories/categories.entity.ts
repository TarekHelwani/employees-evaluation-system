import {
  Table,
  Column,
  Model,
  PrimaryKey,
  HasMany,
  BelongsTo,
  ForeignKey,
  AutoIncrement,
} from 'sequelize-typescript';
import { Employee } from '../employees/employee.entity';

@Table
export class Category extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string;

  @HasMany(() => Level, {
    onDelete: 'CASCADE',
  })
  levels: Level[];
}

@Table
export class Level extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  weight: number;

  @Column
  name: string;

  @ForeignKey(() => Category)
  @Column
  category_id: number;

  @HasMany(() => Employee, {
    onDelete: 'CASCADE',
  })
  employees: Employee[];

  @BelongsTo(() => Category, {
    onDelete: 'CASCADE',
  })
  category: Category;
}
