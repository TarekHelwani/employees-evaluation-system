import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Admin } from '../admins/admin.entity';
import { AdminsService } from '../admins/admins.service';
import { AdminLoginDto } from '../admins/dto/admin-login.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly adminsService: AdminsService,
    private readonly jwtService: JwtService,
  ) {}

  async validateAdmin(adminLoginDto: AdminLoginDto): Promise<Admin> {
    const admin = await this.adminsService.findByEmail(adminLoginDto.email);
    console.log(admin);

    if (admin && (await admin.comparePassword(adminLoginDto.password))) {
      return admin;
    }
    return null;
  }

  login(admin: Admin): string {
    const payload = { username: admin.name, sub: admin.id };
    return this.jwtService.sign(payload);
  }
}
