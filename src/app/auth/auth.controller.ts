import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { jwtConstants } from 'src/constants';
import { Admin } from '../admins/admin.entity';
import { AdminsService } from '../admins/admins.service';
import { AdminLoginDto } from '../admins/dto/admin-login.dto';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guards/jwt-auth.guard';

@Controller()
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly adminsService: AdminsService,
  ) {}

  @Post('auth/login')
  async login(@Body() body) {
    const admin = await this.authService.validateAdmin(
      new AdminLoginDto(body.email, body.password),
    );
    if (!admin) throw new UnauthorizedException('Wrong email or password!');
    return {
      admin,
      token: this.authService.login(admin),
      type: 'Bearer',
      expires_in: jwtConstants.expiresIn,
      expires_at: Date.now() + jwtConstants.expiresIn,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Get('auth/me')
  async getProfile(@Request() req): Promise<Admin> {
    const { id } = req.user;
    const admin = await this.adminsService.findOne(id);
    if (!admin) throw new UnauthorizedException();
    return admin;
  }
}
