import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../database/database.module';
import { ReviewsController, SubReviewsController } from './reviews.controller';
import { ReviewsService, SubReviewsService } from './reviews.service';
import { reviewsProviders, subReviewsProviders } from './reviews.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [ReviewsController],
  providers: [ReviewsService, ...reviewsProviders],
  exports: [ReviewsService],
})
export class ReviewsModule {}

@Module({
  imports: [DatabaseModule, ReviewsModule],
  controllers: [SubReviewsController],
  providers: [SubReviewsService, ...subReviewsProviders],
})
export class SubReviewsModule {}
