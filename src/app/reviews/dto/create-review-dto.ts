export class CreateReviewDto {
  review?: string;
  rank?: number;
  employee_id?: number;
  HR_id?: number;
}
