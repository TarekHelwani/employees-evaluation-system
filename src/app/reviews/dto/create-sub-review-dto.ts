export class CreateSubReviewDto {
  readonly sub_review: string;
  readonly rank: number;
  readonly review_type_id: number;
}
