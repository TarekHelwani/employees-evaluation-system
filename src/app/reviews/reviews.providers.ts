import { Review, SubReview } from './review.entity';
import { Providers } from '../../constants';

export const reviewsProviders = [
  {
    provide: Providers.REVIEW_REPO,
    useValue: Review,
  },
];

export const subReviewsProviders = [
  {
    provide: Providers.SUB_REVIEW_REPO,
    useValue: SubReview,
  },
];
