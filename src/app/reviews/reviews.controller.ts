import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Review, SubReview } from './review.entity';
import { ReviewsService, SubReviewsService } from './reviews.service';
import { CreateReviewDto } from './dto/create-review-dto';
import { CreateSubReviewDto } from './dto/create-sub-review-dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { ReviewTypes } from 'src/constants';

@UseGuards(JwtAuthGuard)
@Controller('reviews')
export class ReviewsController {
  constructor(private readonly reviewsService: ReviewsService) {}

  @Get()
  async findAll(@Query('employee') employeeId): Promise<Review[]> {
    return this.reviewsService.findAll(employeeId);
  }

  @Post('/create/:id')
  create(
    @Body() createReviewDto: CreateReviewDto,
    @Request() req,
    @Param('id') employeeId,
  ): Promise<Review> {
    return this.reviewsService.create(createReviewDto, req.user.id, employeeId);
  }

  @Get(':id')
  async show(@Param('id') reviewId) {
    const review = await this.reviewsService.findOne(reviewId);
    return {
      id: review.id,
      review: review.review,
      HR_id: review.HR_id,
      employee_id: review.employee_id,
      rank: review.rank,
      createdAt: review.createdAt,
      subReviews: review.subReviews.map((sub) => {
        return {
          sub_review: sub.sub_review,
          review_type: ReviewTypes[sub.review_type_id],
          rank: sub.rank,
          rank_str:
            sub.rank >= 0 && sub.rank < 0.2
              ? 'Very Bad'
              : sub.rank >= 0.2 && sub.rank < 0.4
              ? 'Bad'
              : sub.rank >= 0.4 && sub.rank < 0.6
              ? 'Neutral'
              : sub.rank >= 0.6 && sub.rank < 0.8
              ? 'Good'
              : sub.rank >= 0.8 && sub.rank <= 1
              ? 'Very Good'
              : 'Unknown',
        };
      }),
      employee: { ...review.employee.toJSON() },
      admin: {
        name: review.admin.name,
      },
    };
  }

  @Delete(':id')
  async delete(@Param('id') reviewId: number) {
    return await this.reviewsService.delete(reviewId);
  }
}

@UseGuards(JwtAuthGuard)
@Controller('sub-reviews')
export class SubReviewsController {
  constructor(private readonly sub_reviewsService: SubReviewsService) {}

  @Post('/create/:id')
  create(
    @Body() createSubReviewsDto: CreateSubReviewDto[],
    @Request() req,
    @Param('id') employeeId,
  ): Promise<SubReview[]> {
    return this.sub_reviewsService.create(
      createSubReviewsDto,
      req.user.id,
      employeeId,
    );
  }
}
