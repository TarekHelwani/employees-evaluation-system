import {
  AutoIncrement,
  BelongsTo,
  Column,
  ForeignKey,
  HasMany,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';
import { Admin } from '../admins/admin.entity';
import { Employee } from '../employees/employee.entity';

@Table
export class Review extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  review: string;

  @ForeignKey(() => Admin)
  @Column
  HR_id: number;

  @ForeignKey(() => Employee)
  @Column
  employee_id: number;

  @Column
  rank: number;

  @BelongsTo(() => Employee, {
    onDelete: 'CASCADE',
  })
  employee: Employee;

  @BelongsTo(() => Admin, {
    onDelete: 'CASCADE',
  })
  admin: Admin;

  @HasMany(() => SubReview, {
    onDelete: 'CASCADE',
  })
  subReviews: SubReview[];
}

@Table
export class SubReview extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  sub_review: string;

  @ForeignKey(() => Review)
  @Column
  review_id: number;

  @Column
  review_type_id: number;

  @Column
  rank: number;

  @BelongsTo(() => Review, {
    onDelete: 'CASCADE',
  })
  review: Review;
}
