import { Injectable, Inject } from '@nestjs/common';
import { Providers } from '../../constants';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { error, log, Logger } from 'winston';
import { Review, SubReview } from './review.entity';
import { Employee } from '../employees/employee.entity';
import { CreateReviewDto } from './dto/create-review-dto';
import { CreateSubReviewDto } from './dto/create-sub-review-dto';
import { Admin } from '../admins/admin.entity';

@Injectable()
export class ReviewsService {
  constructor(
    @Inject(Providers.REVIEW_REPO)
    private reviewsRepository: typeof Review,
    @Inject(WINSTON_MODULE_PROVIDER)
    private readonly logger: Logger,
  ) {}

  async findAll(employeeId: number): Promise<Review[]> {
    return new Promise<Array<Review>>(
      // eslint-disable-next-line @typescript-eslint/ban-types
      (resolve: Function, reject: Function) => {
        return Review.findAll({
          include: [Employee, Admin],
        })
          .then((reviews: Array<Review>) => {
            this.logger.info(
              `Retrieved reviews for employee id ${employeeId}.`,
            );
            resolve(reviews);
          })
          .catch((error: Error) => {
            this.logger.error(error.message);
            reject(error);
          });
      },
    );
  }

  async delete(reviewId: number) {
    return await Review.destroy({ where: { id: reviewId } });
  }

  async create(
    createReviewDto: CreateReviewDto,
    authId: number,
    employeeId: number,
  ): Promise<Review> {
    return new Promise<Review>(
      // eslint-disable-next-line @typescript-eslint/ban-types
      (resolve: Function, reject: Function) => {
        createReviewDto.HR_id = authId;
        createReviewDto.employee_id = employeeId;
        return Review.create(createReviewDto)
          .then((review: Review) => {
            this.logger.info(`Review has been created.`);
            resolve(review);
          })
          .catch((error: Error) => {
            this.logger.error(error.message);
            reject(error);
          });
      },
    );
  }

  async findOne(reviewId: number): Promise<Review> {
    return Review.findOne({
      where: { id: reviewId },
      include: [SubReview, Employee, Admin],
    });
  }
}

@Injectable()
export class SubReviewsService {
  constructor(
    @Inject(Providers.SUB_REVIEW_REPO)
    private subReviewsRepository: typeof SubReview,
    private reviewsService: ReviewsService,
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
  ) {}

  async create(
    createSubReviewsDto: CreateSubReviewDto[],
    authId: number,
    employeeId: number,
  ): Promise<SubReview[]> {
    return new Promise<SubReview[]>(
      // eslint-disable-next-line @typescript-eslint/ban-types
      async (resolve: Function, reject: Function) => {
        const subReviews: SubReview[] = [];
        let totalReview = '';
        let totalRank = 0.0;
        for (const subReview of createSubReviewsDto) {
          await SubReview.create(subReview)
            .then((subReview: SubReview) => {
              subReviews.push(subReview);
              totalReview += subReview.sub_review + ',';
              totalRank += subReview.rank;
            })
            .catch((error: Error) => {
              reject(error);
            });
        }
        totalReview = totalReview.slice(0, -1);
        const review: CreateReviewDto = {};
        review.review = totalReview;
        review.rank = totalRank;
        await this.reviewsService
          .create(review, authId, employeeId)
          .then((review: Review) => {
            subReviews.forEach((subReview: SubReview) => {
              subReview.update({ review_id: review.id });
            });
          });
        resolve(subReviews);
      },
    );
  }
}
