export interface DatabaseConfig {
  DB_CONNECTION: string;
  DB_HOST: string;
  DB_PORT: number;
  DB_DATABASE: string;
  DB_USERNAME: string;
  DB_PASSWORD: string;
}

export const databaseConfig = {
  DB_CONNECTION: 'mysql',
  DB_HOST: 'localhost',
  DB_PORT: 3306,
  DB_DATABASE: 'employees_evaluation_system',
  DB_USERNAME: 'root',
  DB_PASSWORD: '',
};
