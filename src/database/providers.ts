import { Sequelize } from 'sequelize-typescript';
import { Admin } from 'src/app/admins/admin.entity';
import { sequelizeConfig } from './sequelize';
import { Category, Level } from '../app/categories/categories.entity';
import { Review, SubReview } from '../app/reviews/review.entity';
import { Employee } from '../app/employees/employee.entity';
import { Promotion } from '../app/promotions/promotion.entity';

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize(sequelizeConfig);
      sequelize.addModels([
        Employee,
        Admin,
        Category,
        Level,
        Review,
        SubReview,
        Promotion,
      ]);
      await sequelize.sync();
      return sequelize;
    },
  },
];
