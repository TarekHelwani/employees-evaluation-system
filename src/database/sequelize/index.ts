import { Dialect } from 'sequelize/types';
import { databaseConfig } from 'src/config/db';

const {
  DB_CONNECTION,
  DB_HOST,
  DB_PORT,
  DB_DATABASE,
  DB_USERNAME,
  DB_PASSWORD,
} = databaseConfig;

interface SequelizeConfig {
  dialect: Dialect;
  host: string;
  port: number;
  username: string;
  password: string;
  database: string;
}

export const sequelizeConfig: SequelizeConfig = {
  dialect: DB_CONNECTION as Dialect,
  host: DB_HOST,
  port: DB_PORT,
  username: DB_USERNAME,
  password: DB_PASSWORD,
  database: DB_DATABASE,
};
